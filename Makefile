all:
	npm install
	npm run build
	go get .
	go build

clean:
	rm -rf node_modules dist lowry

test:
	go test ./...
