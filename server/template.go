package server

import (
	"html/template"
	"log"
	"net/http"
)

type responseT struct {
	User    string
	IsAdmin bool
	Section string
	Data    interface{}
	w       http.ResponseWriter
	r       *http.Request
	tmpl    *template.Template
}

func initTemplate() *template.Template {
	return template.Must(template.ParseFiles(
		"tmpl/403.html",
		"tmpl/404.html",
		"tmpl/500.html",
		"tmpl/header.html",
		"tmpl/footer.html",
		"tmpl/navbar.html",
		"tmpl/login.html",
		"tmpl/index.html",
		"tmpl/password.html",
		"tmpl/user.html",
		"tmpl/users.html",
		"tmpl/adduser.html",
		"tmpl/group.html",
		"tmpl/groups.html",
	))
}

func (s *server) newResponse(template string, w http.ResponseWriter, r *http.Request) responseT {
	session := s.sess.get(w, r)
	user := ""
	admin := false
	if session != nil {
		user = session.user
		admin = s.isAdmin(user)
	}
	return responseT{
		User:    user,
		IsAdmin: admin,
		Section: template,
		Data:    nil,
		w:       w,
		r:       r,
		tmpl:    s.tmpl,
	}
}

func (r *responseT) execute(data interface{}) {
	r.Data = data
	if err := r.tmpl.ExecuteTemplate(r.w, r.Section+".html", r); err != nil {
		log.Println("An error ocurred loading template '", r.Section, "': ", err)
		r.tmpl.ExecuteTemplate(r.w, "500.html", r)
	}
}

func (s *server) isAdmin(user string) bool {
	return s.ldap.InGroup(user, "adm")
}
