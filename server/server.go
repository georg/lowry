package server

import (
	"html/template"
	"net/http"

	"0xacab.org/sindominio/lowry/ldap"
	"github.com/gorilla/mux"
)

type server struct {
	ldap *ldap.Ldap
	sess *sessionStore
	tmpl *template.Template
}

// Serve lowry web site
func Serve(addr string, l *ldap.Ldap) error {
	var s server
	s.ldap = l
	s.sess = initSessionStore()
	s.tmpl = initTemplate()

	r := mux.NewRouter()
	var notFoundFunc http.HandlerFunc
	notFoundFunc = s.notFoundHandler
	r.NotFoundHandler = notFoundFunc

	r.HandleFunc("/", s.homeHandler)
	r.HandleFunc("/login/", s.loginHandler)
	r.HandleFunc("/logout/", s.logoutHandler).Methods("POST")
	r.HandleFunc("/password/", s.passwordHandler)
	r.HandleFunc("/users/", s.usersHandler)
	r.HandleFunc("/users/{name}", s.userHandler)
	r.HandleFunc("/users/{name}/password/", s.passwdadmHandler).Methods("POST")
	r.HandleFunc("/users/{name}/shell/", s.shellHandler).Methods("POST")
	r.HandleFunc("/adduser/", s.addUserHandler)
	r.HandleFunc("/groups/", s.groupsHandler)
	r.HandleFunc("/groups/{name}", s.groupHandler)
	r.HandleFunc("/groups/{name}/add/", s.addUserGroupHandler).Methods("POST")
	r.HandleFunc("/groups/{name}/del/", s.delUserGroupHandler).Methods("POST")

	r.HandleFunc("/bundle.js", func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "dist/bundle.js") })
	r.HandleFunc("/style.css", func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "dist/style.css") })
	r.Handle("/img/{img}", http.StripPrefix("/img/", http.FileServer(http.Dir("img"))))

	return http.ListenAndServe(addr, r)
}

func (s *server) notFoundHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	response := s.newResponse("404", w, r)
	response.execute(nil)
}

func (s *server) forbiddenHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusForbidden)
	response := s.newResponse("403", w, r)
	response.execute(nil)
}

func (s *server) errorHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusInternalServerError)
	response := s.newResponse("500", w, r)
	response.execute(nil)
}
