package server

import (
	"log"
	"net/http"

	"0xacab.org/sindominio/lowry/ldap"
	"github.com/gorilla/mux"
)

func (s *server) usersHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("users", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to list users")
		s.forbiddenHandler(w, r)
		return
	}

	users, err := s.ldap.ListUsers()
	if err != nil {
		log.Println("An error ocurred retrieving user list: ", err)
		s.errorHandler(w, r)
		return
	}
	response.execute(users)
}

func (s *server) userHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userName := vars["name"]
	response := s.newResponse("user", w, r)
	if !response.IsAdmin && userName != response.User {
		log.Println("Non admin attemp to list users")
		s.forbiddenHandler(w, r)
		return
	}

	user, err := s.ldap.GetUser(userName)
	if err != nil {
		log.Println("An error ocurred retrieving user '", userName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	mainGroup, err := s.ldap.GetGID(user.GID)
	if err != nil {
		log.Println("An error ocurred retrieving user '", userName, "' main group: ", err)
	}
	groups, err := s.ldap.UserGroups(userName)
	if err != nil {
		log.Println("An error ocurred retrieving user '", userName, "' groups: ", err)
	}
	data := struct {
		User      ldap.User
		MainGroup ldap.Group
		Groups    []ldap.Group
	}{user, mainGroup, groups}
	response.execute(data)
}

func (s *server) addUserHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("adduser", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to add user")
		s.forbiddenHandler(w, r)
		return
	}

	if r.Method != "POST" {
		response.execute("")
		return
	}

	name := r.FormValue("name")
	pass := r.FormValue("password")

	if name == "" || pass == "" {
		response.execute("empty")
		return
	}
	_, err := s.ldap.GetUser(name)
	if err == nil {
		log.Println("Can't create user ", name, ": already exist")
		response.execute("exsist")
		return
	}

	err = s.ldap.AddGroup(name)
	if err != nil {
		log.Println("Error adding group: ", err)
		s.errorHandler(w, r)
		return
	}
	group, err := s.ldap.GetGroup(name)
	if err != nil {
		log.Println("Error getting group: ", err)
		s.errorHandler(w, r)
		return
	}

	err = s.ldap.AddUser(name, pass, group.GID)
	if err != nil {
		log.Println("Error adding user: ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/users/"+name, http.StatusFound)
}

func (s *server) groupsHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("groups", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to list users")
		s.forbiddenHandler(w, r)
		return
	}

	groups, err := s.ldap.ListGroups()
	if err != nil {
		log.Println("An error ocurred retrieving group list: ", err)
		s.errorHandler(w, r)
		return
	}
	response.execute(groups)
}

func (s *server) groupHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	groupName := vars["name"]
	response := s.newResponse("group", w, r)
	if !response.IsAdmin && !s.ldap.InGroup(response.User, groupName) {
		log.Println("Non admin attemp to list users")
		s.forbiddenHandler(w, r)
		return
	}

	group, err := s.ldap.GetGroup(groupName)
	if err != nil {
		log.Println("An error ocurred retrieving group '", groupName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	response.execute(group)
}

func (s *server) passwdadmHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to change user password")
		s.forbiddenHandler(w, r)
		return
	}

	vars := mux.Vars(r)
	userName := vars["name"]
	pass := r.FormValue("password")
	err := s.ldap.ChangePassAdmin(userName, pass)
	if err != nil {
		log.Println("An error ocurred changing password of '", userName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/users/"+userName, http.StatusFound)
}

func (s *server) shellHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to change user password")
		s.forbiddenHandler(w, r)
		return
	}

	vars := mux.Vars(r)
	userName := vars["name"]
	shell := r.FormValue("shell")
	err := s.ldap.ChangeShell(userName, shell)
	if err != nil {
		log.Println("An error ocurred changing shell of '", userName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/users/"+userName, http.StatusFound)
}

func (s *server) addUserGroupHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to add user to group")
		s.forbiddenHandler(w, r)
		return
	}

	vars := mux.Vars(r)
	groupName := vars["name"]
	user := r.FormValue("user")
	err := s.ldap.AddUserGroup(user, groupName)
	if err != nil {
		log.Println("An error ocurred adding user '", user, "' to ", groupName, ": ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/groups/"+groupName, http.StatusFound)
}

func (s *server) delUserGroupHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to del user to group")
		s.forbiddenHandler(w, r)
		return
	}

	vars := mux.Vars(r)
	groupName := vars["name"]
	user := r.FormValue("user")
	err := s.ldap.DelUserGroup(user, groupName)
	if err != nil {
		log.Println("An error ocurred del user '", user, "' to ", groupName, ": ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/groups/"+groupName, http.StatusFound)
}
