package server

import (
	"net/http"
)

func (s *server) homeHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("index", w, r)
	if response.User == "" {
		response = s.newResponse("login", w, r)
	}
	response.execute(false)
}

func (s *server) loginHandler(w http.ResponseWriter, r *http.Request) {
	user := r.FormValue("user")
	pass := r.FormValue("password")

	err := s.ldap.ValidateUser(user, pass)
	if err != nil {
		response := s.newResponse("login", w, r)
		response.execute(true)
		return
	}

	s.sess.set(user, w, r)
	http.Redirect(w, r, "/", http.StatusFound)
}

func (s *server) logoutHandler(w http.ResponseWriter, r *http.Request) {
	s.sess.del(w, r)
	http.Redirect(w, r, "/", http.StatusFound)
}

func (s *server) passwordHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("password", w, r)
	if response.User == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if r.Method != "POST" {
		response.execute("")
		return
	}

	oldpass := r.FormValue("oldpass")
	pass := r.FormValue("password")
	pass2 := r.FormValue("password2")
	if pass != pass2 {
		response.execute("")
		return
	}

	err := s.ldap.ChangePass(response.User, oldpass, pass)
	if err != nil {
		response.execute("WrongOldpass")
	} else {

		response.execute("PassChanged")
	}
}
