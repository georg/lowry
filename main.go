package main

import (
	"log"

	"0xacab.org/sindominio/lowry/ldap"
	"0xacab.org/sindominio/lowry/server"
	"github.com/namsral/flag"
)

func main() {
	var (
		ldapaddr = flag.String("ldapaddr", "localhost:389", "LDAP server address and port")
		domain   = flag.String("domain", "", "LDAP domain components")
		ldappass = flag.String("ldappass", "", "Password of the LDAP `admin' user")
		homepath = flag.String("homepath", "/home/", "Path to the user homes")
		httpaddr = flag.String("httpaddr", ":8080", "Web server address and port")
		ro       = flag.Bool("ro", false, "Read-Only mode")
	)
	flag.String(flag.DefaultConfigFlagname, "/etc/lowry.conf", "Path to configuration file")
	flag.Parse()

	l := ldap.Ldap{
		Addr:     *ldapaddr,
		Domain:   *domain,
		Pass:     *ldappass,
		HomePath: *homepath,
		RO:       *ro,
	}
	err := l.Init()
	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(server.Serve(*httpaddr, &l))
}
