package ldap

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/go-ldap/ldap"
)

// Group has the ldap data of the group
type Group struct {
	Name    string
	GID     int
	Members []string
}

// InGroup checks if user is part of group
func (l Ldap) InGroup(user string, group string) bool {
	conn, err := l.connect()
	if err != nil {
		return false
	}
	defer conn.Close()

	searchRequest := ldap.NewSearchRequest(
		l.groupDN(group),
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectClass=posixGroup)(memberUid=%s))", ldap.EscapeFilter(user)),
		[]string{"dn"},
		nil,
	)
	sr, err := conn.Search(searchRequest)
	return err == nil && len(sr.Entries) > 0
}

// GetGroup returns the group matching the name
func (l Ldap) GetGroup(name string) (Group, error) {
	filter := fmt.Sprintf("(&(objectClass=posixGroup)(cn=%s))", ldap.EscapeFilter(name))
	groups, err := l.searchGroup(filter)
	if err != nil {
		return Group{}, err
	}
	if len(groups) == 0 {
		return Group{}, errors.New("Can't find group " + name)
	}
	return groups[0], nil

}

// GetGID returns the group matching the gid
func (l Ldap) GetGID(gid int) (Group, error) {
	filter := fmt.Sprintf("(&(objectClass=posixGroup)(gidNumber=%d))", gid)
	groups, err := l.searchGroup(filter)
	if err != nil {
		return Group{}, err
	}
	if len(groups) == 0 {
		return Group{}, errors.New("Can't find group " + strconv.Itoa(gid))
	}
	return groups[0], nil
}

// ListGroups returns all groups in ldap with members
func (l Ldap) ListGroups() ([]Group, error) {
	filter := "(&(objectClass=posixGroup)(memberUid=*))"
	return l.searchGroup(filter)
}

// UserGroups returns a list of groups the user is member of
func (l Ldap) UserGroups(user string) ([]Group, error) {
	filter := fmt.Sprintf("(&(objectClass=posixGroup)(memberUid=%s))", ldap.EscapeFilter(user))
	return l.searchGroup(filter)
}

// AddGroup adds the group to ldap
func (l Ldap) AddGroup(group string) error {
	if _, err := l.GetGroup(group); err == nil {
		return errors.New("Group '" + group + "' already exist, can't create it")
	}

	gid, err := l.getLastID("gidNumber")
	if err != nil {
		return err
	}
	gid++

	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	dn := l.groupDN(group)
	addRequest := ldap.NewAddRequest(dn)
	addRequest.Attribute("cn", []string{ldap.EscapeFilter(group)})
	addRequest.Attribute("objectClass", []string{"top", "posixGroup"})
	addRequest.Attribute("gidNumber", []string{strconv.Itoa(gid)})
	return conn.Add(addRequest)
}

// DelGroup removes the group in ldap
func (l Ldap) DelGroup(group string) error {
	return l.del(l.groupDN(group))
}

// AddUserGroup add user into the group members
func (l Ldap) AddUserGroup(user string, group string) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	modifyRequest := ldap.NewModifyRequest(l.groupDN(group))
	modifyRequest.Add("memberUid", []string{ldap.EscapeFilter(user)})
	return conn.Modify(modifyRequest)
}

// DelUserGroup removes the user from the group members
func (l Ldap) DelUserGroup(user string, group string) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	modifyRequest := ldap.NewModifyRequest(l.groupDN(group))
	modifyRequest.Delete("memberUid", []string{ldap.EscapeFilter(user)})
	return conn.Modify(modifyRequest)
}

func (l Ldap) groupDN(group string) string {
	groupStr := ldap.EscapeFilter(group)
	return fmt.Sprintf("cn=%s,ou=group,%s", groupStr, l.DC)
}

func (l Ldap) searchGroup(filter string) ([]Group, error) {
	conn, err := l.connect()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	searchRequest := ldap.NewSearchRequest(
		"ou=group,"+l.DC,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		filter,
		[]string{"cn", "memberUid", "gidNumber"},
		nil,
	)
	sr, err := conn.Search(searchRequest)
	if err != nil {
		return nil, err
	}
	groups := []Group{}
	for _, entry := range sr.Entries {
		groups = append(groups, newGroup(entry))
	}
	return groups, nil
}

func newGroup(entry *ldap.Entry) Group {
	gid, _ := strconv.Atoi(entry.GetAttributeValue("gidNumber"))
	return Group{
		Name:    entry.GetAttributeValue("cn"),
		Members: entry.GetAttributeValues("memberUid"),
		GID:     gid,
	}
}
