package ldap

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/go-ldap/ldap"
)

// Ldap configuration
type Ldap struct {
	Addr       string
	Domain     string
	Pass       string
	HomePath   string
	DC         string
	MailDomain string
	RO         bool
}

// Init the Ldap connection
func (l *Ldap) Init() error {
	if l.DC == "" {
		l.DC = "dc=" + strings.Replace(l.Domain, ".", ",dc=", -1)
	}
	if l.MailDomain == "" {
		l.MailDomain = l.Domain
	}

	conn, err := l.connect()
	if err != nil {
		log.Println("Failed to connect ", l.DC, ": ", err)
		return err
	}
	defer conn.Close()
	return nil
}

func (l Ldap) connect() (*ldap.Conn, error) {
	conn, err := ldap.Dial("tcp", l.Addr)
	if err != nil {
		return nil, err
	}
	err = conn.Bind("cn=admin,"+l.DC, l.Pass)
	return conn, err
}

func (l Ldap) getLastID(attribute string) (int, error) {
	const highestID = 20000

	conn, err := l.connect()
	if err != nil {
		return 0, err
	}
	defer conn.Close()

	searchRequest := ldap.NewSearchRequest(
		l.DC,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(%s=*))", attribute),
		[]string{attribute},
		nil,
	)
	sr, err := conn.Search(searchRequest)
	if err != nil {
		return 0, err
	}
	id := 0
	for _, entry := range sr.Entries {
		valueID, err := strconv.Atoi(entry.GetAttributeValue(attribute))
		if err == nil && valueID > id && valueID < highestID {
			id = valueID
		}
	}
	return id, nil
}

func (l Ldap) del(dn string) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	delRequest := ldap.NewDelRequest(dn, nil)
	return conn.Del(delRequest)
}
